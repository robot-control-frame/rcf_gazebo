# rcf_gazebo

## Overview

用于控制算法仿真验证

![](doc/image/balance.png)
![](doc/image/mecanum.png)
![](doc/image/omni.png)

## 用法示例

### 1. 启动

```shell
roslaunch rcf_gazebo balance_lqr_empty_world.launch
```

### 2. 使用 rqt 发布控制指令

```shell
rqt
```

#### 2.1 发布 `/controllers/chassis_controller/command`启动底盘

推荐测试参数：  
`accel linear x` - 5  
`accel linear y` - 5  
`accel angular z` - 5

#### 2.2. 发布 `cmd_vel` 控制底盘移动

![](doc/image/lqr_balance.gif)